/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  PhytonControlView
  @author  Abraham Carrera, Daniel Del Olmo
  @date    02-2018
  @version 3.0
*/

#include "../include/python_mission_control_window_view.h"

PythonMissionControlWindowView::PythonMissionControlWindowView(int argc, char** argv, QWidget* parent)
  : QWidget(parent), ui(new Ui::PythonMissionControlWindowView)
{
  setWindowIcon(QIcon(":/images/images/python_control.png"));
  setWindowTitle("Python Mission Control Window");
  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);

  ui->setupUi(this);  // connects all ui's triggers

  // Add the control panel widget.
  python_control = new PythonMissionControlWindow(this);
  ui->gridLayout->addWidget(python_control);

  // Layout
  namespace pt = boost::property_tree;

  std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                        "graphical_user_interface/layouts/layout.json");

  pt::read_json(layout_dir, root);

  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();

  int y0 = screenGeometry.height() / 2;
  int x0 = screenGeometry.width() / 2;
  int height = root.get<int>("PYTHON_CONTROL.height");
  int width = root.get<int>("PYTHON_CONTROL.width");

  this->resize(width, height);
  this->move(x0 + root.get<int>("PYTHON_CONTROL.position.x"), y0 + root.get<int>("PYTHON_CONTROL.position.y"));

  // Settings connections
  setUp();
}

PythonMissionControlWindowView::~PythonMissionControlWindowView()
{
  delete ui;
  delete python_control;
}

PythonMissionControlWindow* PythonMissionControlWindowView::getPythonMissionControlWindow()
{
  return python_control;
}
void PythonMissionControlWindowView::closeEvent(QCloseEvent* event)
{
  window_event_msg.window = aerostack_msgs::WindowEvent::PYTHON_CONTROL;
  window_event_msg.event = aerostack_msgs::WindowEvent::CLOSE;
  window_event_pub.publish(window_event_msg);
}

void PythonMissionControlWindowView::keyPressEvent(QKeyEvent* e)
{
  // std::cout<<"KEY PRESSED"<<std::endl;
  QWidget::keyPressEvent(e);
  if (!isAKeyPressed && acceptedKeys.contains(e->key()))
  {
    isAKeyPressed = true;

    switch (e->key())
    {
      case Qt::Key_T:
      {
        if (!e->isAutoRepeat())
        {
          acceptedKeys.insert(0x54, true);

         aerostack_msgs::RequestBehaviorActivation::Request msg;
        aerostack_msgs::RequestBehaviorActivation::Response res;
        aerostack_msgs::BehaviorCommandPriority behavior;
        behavior.name = "TAKE_OFF";
        behavior.priority = 3;
        msg.behavior = behavior;
        activate_behavior_srv.call(msg,res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;
          std::this_thread::sleep_for(std::chrono::milliseconds(1000));
          python_control->displayMissionStarted();
        }
        break;
      }
      case Qt::Key_Y:
      {
        if (!e->isAutoRepeat())
        {
          acceptedKeys.insert(0x59, true);
        aerostack_msgs::RequestBehaviorActivation::Request msg;
        aerostack_msgs::RequestBehaviorActivation::Response res;
        aerostack_msgs::BehaviorCommandPriority behavior;
        behavior.name = "LAND";
        behavior.priority = 3;
        msg.behavior = behavior;
        activate_behavior_srv.call(msg,res);
          activate_behavior_srv.call(msg, res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;
          python_control->displayMissionCompleted();
        }
        break;
      }
       case Qt::Key_H:
      {
        acceptedKeys.insert(0x48, true);
        aerostack_msgs::RequestBehaviorActivation::Request msg;
        aerostack_msgs::RequestBehaviorActivation::Response res;
        aerostack_msgs::BehaviorCommandPriority behavior;
        behavior.name = "KEEP_HOVERING_WITH_PID_CONTROL";
        behavior.priority = 3;
        msg.behavior = behavior;
        activate_behavior_srv.call(msg,res);
        if(!res.ack)
          std::cout << res.error_message << std::endl;
        break;
      }
      case Qt::Key_Right:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000014, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.priority = 3;
          behavior.arguments = "direction: \"RIGHT\"\nspeed: 0.4";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Left:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000012, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"LEFT\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Down:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000015, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"BACKWARD\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
          break;
      }
      case Qt::Key_Up:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000013, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"FORWARD\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
          break;
      }
      case Qt::Key_Q:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x51, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"UP\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_A:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x41, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"DOWN\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Z:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x5a, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "ROTATE_WITH_PID_CONTROL";
          behavior.arguments = "relative_angle: +179";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_X:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x58, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "ROTATE_WITH_PID_CONTROL";
          behavior.arguments = "relative_angle: -179";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
    }
  }
}

void PythonMissionControlWindowView::keyReleaseEvent(QKeyEvent* e)
{  // std::cout<<"KEY RELEASED"<<std::endl;
  if (e->isAutoRepeat() || !acceptedKeys.contains(e->key()))
  {
    isAKeyPressed = false;
    e->ignore();
  }
  else if (acceptedKeys.contains(e->key()) && acceptedKeys.value(e->key()))
  {
    if ((e->key() == 0x52))
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
     acceptedKeys.insert(e->key(), false);
      aerostack_msgs::RequestBehaviorActivation::Request msg;
      aerostack_msgs::RequestBehaviorActivation::Response res;
      aerostack_msgs::BehaviorCommandPriority behavior;
      behavior.name = "KEEP_HOVERING_WITH_PID_CONTROL";
      msg.behavior = behavior;
      behavior.priority = 1;
    if (e->key() != Qt::Key_Y && e->key() != Qt::Key_T && e->key() != Qt::Key_R)
      // activate_behavior_srv.call(msg,res);
      if (!res.ack)
        std::cout << res.error_message << std::endl;
    isAKeyPressed = false;
    QWidget::keyReleaseEvent(e);
  }
  else
  {
    isAKeyPressed = false;
    e->ignore();
    QWidget::keyReleaseEvent(e);
  }
}

void PythonMissionControlWindowView::setUp()
{
  n.param<std::string>("robot_namespace", drone_id_namespace, "drone1");

  n.param<std::string>("window_event_topic", window_event_topic, "window_event");


  // Subscribers
  window_event_sub = n.subscribe("/" + drone_id_namespace + "/" + window_event_topic, 10,
                                   &PythonMissionControlWindowView::windowOpenCallback, this);

  // Publishers
  window_event_pub =
      n.advertise<aerostack_msgs::WindowEvent>("/" + drone_id_namespace + "/" + window_event_topic, 1, true);

  n.param<std::string>("activate_behavior", activate_behavior, "request_behavior_activation");
  activate_behavior_srv =
      n.serviceClient<aerostack_msgs::RequestBehaviorActivation>("/" + drone_id_namespace + "/" + activate_behavior);

  n.param<std::string>("initiate_behaviors", initiate_behaviors, "initiate_behaviors");
  initiate_behaviors_srv =
      n.serviceClient<droneMsgsROS::InitiateBehaviors>("/" + drone_id_namespace + "/" + initiate_behaviors);
  droneMsgsROS::InitiateBehaviors msg;
  initiate_behaviors_srv.call(msg);

  // Key settings
  isAKeyPressed = false;

  setFocusPolicy(Qt::StrongFocus);
  acceptedKeys.insert(0x01000012, false);  // Tecla UP
  acceptedKeys.insert(0x01000013, false);  // Tecla DOWN
  acceptedKeys.insert(0x01000014, false);  // Tecla LEFT
  acceptedKeys.insert(0x01000015, false);  // Tecla RIGHT
  acceptedKeys.insert(0x51, false);        // Tecla Q
  acceptedKeys.insert(0x41, false);        // Tecla A
  acceptedKeys.insert(0x54, false);        // Tecla T
  acceptedKeys.insert(0x59, false);        // Tecla Y
  acceptedKeys.insert(0x48, false);        // Tecla H
  acceptedKeys.insert(0x5a, false);        // Tecla Z
  acceptedKeys.insert(0x58, false);        // Tecla X
  acceptedKeys.insert(0x52, false);        // Tecla R

  
}

void PythonMissionControlWindowView::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void PythonMissionControlWindowView::windowOpenCallback(const aerostack_msgs::WindowEvent& msg)
{
 

  if (msg.window == aerostack_msgs::WindowEvent::TELEOPERATION_CONTROL ||
      msg.window == aerostack_msgs::WindowEvent::ALPHANUMERIC_INTERFACE_CONTROL ||
      msg.window == aerostack_msgs::WindowEvent::BEHAVIOR_TREE_INTERPRETER)
  {
    window_event_msg.window = aerostack_msgs::WindowEvent::PYTHON_CONTROL;
    window_event_msg.event = aerostack_msgs::WindowEvent::CLOSE;
    window_event_pub.publish(window_event_msg);
    killMe();
  }

  if (msg.window == aerostack_msgs::WindowEvent::INTEGRATED_VIEWER && msg.event == aerostack_msgs::WindowEvent::MINIMIZE)
    showMinimized();
}
